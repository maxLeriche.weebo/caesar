import string

def check_iteration(iteration):
    """
        verification de si le nombre d'iteration est bon
    """
    if iteration>20 or iteration<1:
        raise Exception("Entre 1 et 20.")
def check_phrase(phrase):
    """
        verification de si l'utilisateur à rentré une phrase
    """
    if phrase is None:
        raise Exception("Phrase ne doit pas être vide.")

def code_non_circu(phrase, iteration):
    """
        :param phrase: phrase saisie par l'utilisateur
        :param iterarion: variable correspondant au chiffre de codage

        fonction de codage qui va ajouter +iteration à la table ASCII caractère par caractère de phrase non circulaire

        :Example:
        >>> code("abc", 1)
        'bcd'
        >>> code ("abc", 3)
        'def'
    """
    check_iteration(iteration)
    check_phrase(phrase)
    resultat = ''
    for character in phrase:
        resultat=resultat+chr(ord(character)+iteration)
    return resultat

def decode_non_circu(phrase, iteration):
    """
        :param phrase: phrase saisie par l'utilisateur
        :param iterarion: variable correspondant au chiffre de codage

        fonction de codage qui va ajouter +iteration à la table ASCII caractère par caractère de phrase non-circulaire

        :Example:
        >>> code("bcd", 1)
        'abc'
        >>> code ("def", 3)
        'abc'
    """
    check_iteration(iteration)
    check_phrase(phrase)
    resultat = ''
    for character in phrase:
        resultat=resultat+chr(ord(character)-iteration)
    return resultat

def code_circu(phrase, iteration):
    """
        :param phrase: phrase saisie par l'utilisateur
        :param iterarion: variable correspondant au chiffre de codage

        fonction de codage qui va ajouter +iteration à la table ASCII caractère par caractère de phrase en mode circulaire

        :Example:
        >>> code("abc", 1)
        'bcd'
        >>> code ("xyz", 1)
        'yza"
    """
    check_iteration(iteration)
    check_phrase(phrase)
    resultat = ''
    for character in phrase:
        if ord(character)+iteration>122 and ord(character)+iteration>90:
            resultat=resultat+chr(ord(character)-26+iteration)
        else : resultat=resultat+chr(ord(character)+iteration)
    return resultat

def decode_circu(phrase, iteration):
    """
        :param phrase: phrase saisie par l'utilisateur
        :param iterarion: variable correspondant au chiffre de codage

        fonction de codage qui va ajouter +iteration à la table ASCII caractère par caractère de phrase en mode circulaire

        :Example:
        >>> code("bcd", 1)
        'abc'
        >>> code ("yza", 1)
        'xyz'
    """
    check_iteration(iteration)
    check_phrase(phrase)
    resultat = ''
    for character in phrase:
        if ord(character)-iteration<97 or ord(character)-iteration<65:
            resultat=resultat+chr(ord(character)+26-iteration)
        else : resultat=resultat+chr(ord(character)-iteration)
        
    return resultat