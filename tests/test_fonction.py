import unittest
from Fonction import *

class FonctionTestCase(unittest.TestCase):

    def test_check_phrase(self):
        with self.assertRaises(Exception):
            check_iteration(None)
        self.assertIsNone(check_phrase(""))
        self.assertIsNone(check_phrase("jojo"))

    def test_check_iteration(self):
        with self.assertRaises(Exception):
            check_iteration(None)
        self.assertIsNone(check_iteration(1))

    def test_cnc(self):
        self.assertEqual(code_non_circu("abc", 1), "bcd")
        self.assertEqual(code_non_circu("abz", 1), "bc{")
    
    def test_dnc(self):
        self.assertEqual(decode_non_circu("bcd", 1), "abc")
        self.assertEqual(decode_non_circu("abz", 1), "`ay")
    
    def test_cc(self):
        self.assertEqual(code_circu("abc", 1), "bcd")
        self.assertEqual(code_circu("abz", 1), "bca")
    
    def test_dc(self):
        self.assertEqual(decode_circu("bcd", 1), "abc")
        self.assertEqual(decode_circu("acd", 1), "zbc")