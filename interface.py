from tkinter.messagebox import *
from tkinter import * 
from tkinter import ttk
from Fonction import *

def callback_code_non_circu():

    value.set(code_non_circu(Textencode.get(),int(increment.get()))) 

def callback_decode_non_circu():
    value2.set(decode_non_circu(Textdecode.get(),int(increment.get())))

def callback_code_circu():

    value.set(code_circu(Textencode.get(),int(increment.get()))) 

def callback_decode_circu():
    value2.set(decode_circu(Textdecode.get(),int(increment.get())))


root = Tk()
root.title("CESAR")
root.minsize(800, 150)
root.columnconfigure(0, weight=2)
root.columnconfigure(2, weight=3)
root.columnconfigure(1, weight=2)
root.columnconfigure(1, weight=2)
root.columnconfigure(4, weight=2)
root.rowconfigure(3,weight=1)


label=Label(root,text="ISEKAI MONSTER")
label.grid(row=1, column=1)
value = "Donne moi une valeur"
value= StringVar(root)
value2=StringVar(root)

Textencode = Entry(root,textvariable=value2)
Textencode.grid(row=2, column=0,padx=10, pady=10, sticky='nsew')

Textdecode = Entry(root, textvariable=value)
Textdecode.grid(row=2, column=2,padx=10, pady=10, sticky='nsew')

increment=ttk.Spinbox(root,from_=1,to=20)
increment.grid(row=3,column=1)

encode_non_circu_btn = Button(root,text='encode non circulaire',command=callback_code_non_circu)
encode_non_circu_btn.grid(row=4, column=0)

decode_non_circu_btn = Button(root,text='decode non circulaire',command=callback_decode_non_circu)
decode_non_circu_btn.grid(row=4, column=1)

encode_circu_btn = Button(root,text='encode circulaire',command=callback_code_circu)
encode_circu_btn.grid(row=4, column=2)

decode_circu_btn = Button(root,text='decode circulaire',command=callback_decode_circu)
decode_circu_btn.grid(row=4, column=3)

root.mainloop()